package INF102.lab6.cheapFlights;

import java.util.List;

import INF102.lab6.graph.WeightedDirectedGraph;

public class CheapestFlight implements ICheapestFlight {


    @Override
    public WeightedDirectedGraph<City, Integer> constructGraph(List<Flight> flights) {
        // node/vertex = city
        // edges = flight from start to destination
        // weight = cost of trip

        WeightedDirectedGraph<City, Integer> graph = new WeightedDirectedGraph<>();
        for (City node : City.CITIES) { // getting nodes which is also represented as the cities
            graph.addVertex(node);
        }

        for (Flight edge : flights) { // loops through collection flights to get edges and cost
            City edge1 = edge.start;
            City edge2 = edge.destination;
            int weight = edge.cost;

            graph.addEdge(edge1, edge2, weight);
        }

        return graph;
    }

    private void searchCheapestFlight(List<Flight> flights, City currentCity, City destination, int currentCost, int stops, int nMaxStops, int[] minCost) { // explores different flight paths from the current city to the final destination
        // update minCost when destination reached
        if (currentCity == destination) {
            if (currentCost < minCost[0]){ // only if the minCost is less than currentcost do we update
                minCost[0] = currentCost;
            }
            return;
        }
        // base case: if we exceed the maximum number of stops, stop searching
        if (stops > nMaxStops){
            return;
        }
        // iterating through flights
        for (Flight flight : flights) {
            if (flight.start == currentCity){
                searchCheapestFlight(flights, flight.destination, destination, currentCost + flight.cost, stops + 1, nMaxStops, minCost);
            }
        }
    }

    @Override
    public int findCheapestFlights(List<Flight> flights, City start, City destination, int nMaxStops) {
        int[] minCost = {Integer.MAX_VALUE};

        searchCheapestFlight(flights, start, destination, 0, 0, nMaxStops, minCost);

        return minCost[0];
    }

}
